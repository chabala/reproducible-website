---
layout: report
year: "2023"
month: "11"
title: "Reproducible Builds in November 2023"
draft: false
date: 2023-12-06 14:57:09
---

[![]({{ "/images/reports/2023-11/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the November 2023 report from the [Reproducible Builds](https://reproducible-builds.org) project!** In these reports we outline the most important things that we have been up to over the past month. As a rather rapid recap, whilst anyone may inspect the source code of free software for malicious flaws, almost all software is distributed to end users as pre-compiled binaries ([more]({{ "/#why-does-it-matter" | relative_url }})).

---

### Reproducible Builds Summit 2023

Between October 31st and November 2nd, we held our [seventh Reproducible Builds Summit]({{ "/events/hamburg2023/" | relative_url }}) in Hamburg, Germany! Amazingly, the [agenda and all notes from all sessions](https://reproducible-builds.org/events/hamburg2023/agenda/) are all online — many thanks to everyone who wrote notes from the sessions.

As a followup on one idea, started at the summit, Alexander Couzens and Holger Levsen started work on a cache (or tailored front-end) for the [*snapshot.debian.org*](https://snapshot.debian.org) service. The general idea is that, when rebuilding Debian, you do not actually need the whole ~140TB of data from *snapshot.debian.org*; rather, only a very small subset of the packages are ever used for for building. It turns out, for `amd64`, `arm64`, `armhf`, `i386`, `ppc64el`, `riscv64` and `s390` for Debian *trixie*, *unstable* and *experimental*, this is only around 500GB — ie. less than 1%. Although the new service not yet ready for usage, it has already provided a promising outlook in this regard. More information is available on [https://rebuilder-snapshot.debian.net](https://rebuilder-snapshot.debian.net) and we hope that this service becomes usable in the coming weeks.

[![]({{ "/images/reports/2023-11/postit-rb-summit-hamburg-20231101_small.jpg#right" | relative_url }})](https://rebuilder-snapshot.debian.net/)

The adjacent picture shows a sticky note authored by Jan-Benedict Glaw at the summit in Hamburg, confirming Holger Levsen's theory that rebuilding all Debian packages needs a very small subset of packages, the text states that 69,200 packages (in Debian *sid*) list 24,850 packages in their `.buildinfo` files, in 8,0200 variations. This little piece of paper was the beginning of *rebuilder-snapshot* and is a direct outcome of the summit!

The Reproducible Builds team would like to thank our event sponsors who include [Mullvad VPN](https://mullvad.net/), [openSUSE](https://www.opensuse.org/), [Debian](https://www.debian.org/), [Software Freedom Conservancy](https://sfconservancy.org/), [Allotropia](https://www.debian.org/) and [Aspiration Tech](https://aspirationtech.org/).

<br>

### *Beyond Trusting FOSS* presentation at SeaGL

[![]({{ "/images/reports/2023-11/seagl.png#right" | relative_url }})](https://seagl.org)

On November 4th, Vagrant Cascadian presented [*Beyond Trusting FOSS*](https://osem.seagl.org/conferences/seagl2023/program/proposals/939) at [SeaGL](https://seagl.org/) in Seattle, WA in the United States. Founded in 2013, SeaGL is a free, grassroots technical summit dedicated to spreading awareness and knowledge about free source software, hardware and culture. The summary of Vagrant's talk mentions that it will:

> […] introduce the concepts of Reproducible Builds, including best practices for developing and releasing software, the tools available to help diagnose issues, and touch on progress towards solving decades-old deeply pervasive fundamental security issues... Learn how to verify and demonstrate trust, rather than simply hoping everything is OK!

Germane to the contents of the talk, the [slides for Vagrant's talk](https://salsa.debian.org/reproducible-builds/reproducible-presentations/-/tree/master/2023-11-04-SeaGL-Beyond-Trusting-FOSS) can be built reproducibly, resulting in a PDF with a SHA1 of `cfde2f8a0b7e6ec9b85377eeac0661d728b70f34` when built on Debian *bookworm* and `c21fab273232c550ce822c4b0d9988e6c49aa2c3` on Debian *sid* at the time of writing.

<br>

### *Human Factors in Software Supply Chain Security*

[![]({{ "/images/reports/2023-11/fourne-sc-agenda-2023.png#right" | relative_url }})](https://ieeexplore.ieee.org/document/10315781)

Marcel Fourné, Dominik Wermke, Sascha Fahl and Yasemin Acar have published an article in a Special Issue of the IEEE's [*Security & Privacy*](https://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=8013) magazine. Entitled *A Viewpoint on Human Factors in Software Supply Chain Security: A Research Agenda*, the paper justifies the need for reproducible builds to reach developers and end-users specifically, and furthermore points out some under-researched topics that we have seen mentioned in interviews. An [author pre-print of the article](https://marcelfourne.de/fourne-sc-agenda-2023.pdf) is available in PDF form.

<br>

### Community updates

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* [Julien Lepiller mentioned](https://lists.reproducible-builds.org/pipermail/rb-general/2023-November/003115.html) that they were interested in translating [our website]({{ "/" | relative_url }}), and provided offered [GNU Guix's translation framework](https://translate.fedoraproject.org/projects/guix/website/nl/) ([Weblate](https://weblate.org/en-gb/)) as a potential model to emulate.

* Bernhard M. Wiedemann posted a positive "[LibreOffice success story](https://lists.reproducible-builds.org/pipermail/rb-general/2023-November/003121.html)" documenting that, after some work:

    > […] today I hold in my hands the first two bit-identical LibreOffice rpm packages. And this is the success I wanted to share with you all today [and] **it makes me feel as if we can solve anything**.

* *kpcyrd* reported on their excellent results with making [`esp32c3` microcontroller firmware reproducible with Rust, repro-env and Arch Linux](https://lists.reproducible-builds.org/pipermail/rb-general/2023-November/003205.html):

    > I chose the `esp32c3` [board] because it has good Rust support from the [`esp-rs` project](https://github.com/esp-rs), and you can get a dev board for about 6-8€. To document my build environment I used [`repro-env`](https://github.com/kpcyrd/repro-env) together with Arch Linux because its [archive](https://archive.archlinux.org/) is very reliable and contains all the different Rust development tools I needed.

* Separate to their work on LibreOffice however, Bernhard M. Wiedemann [also requested assistance](https://lists.reproducible-builds.org/pipermail/rb-general/2023-November/003195.html) with a number of packages that so far refuse to build reproducibly. He writes that "the common theme around them is that they use scheme or lisp to produce binaries with a `dump` command" and hopes that someone may be able to help.

* Finally, Fay Stegerman regrettably reports that she will [no longer be able to work on Android reproducible builds](https://lists.reproducible-builds.org/pipermail/rb-general/2023-November/003201.html) nor update the Reproducible Builds community with the status of reproducibility within F-Droid.

<br>

### openSUSE updates

[![]({{ "/images/reports/2023-11/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Bernhard M. Wiedemann has [created a wiki page](https://en.opensuse.org/openSUSE:Reproducible_openSUSE) outlining an proposal to create a general-purpose Linux distribution which consists of 100% bit-reproducible packages… albeit minus the embedded signature within RPM files. It would be based on openSUSE *Tumbleweed* or, if available, its Slowroll-variant.

In addition, Bernhard posted another [monthly update](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/VWWVEPYEUBBMZN6K7VGUU5KKC7CBSSM2/) for his work elsewhere in openSUSE.

<br>


### Reproducibility-related changes in Debian

[![]({{ "/images/reports/2023-11/debian.png#right" | relative_url }})](https://debian.org/)

As [recently reported in the most recent *Debian Developer News*](https://lists.debian.org/debian-devel-announce/2023/11/msg00003.html), Paul Gevers has integrated a package's reproducibility status into the way Debian 'migrates' packages into the next stable release. For the `amd64`, `arm64`, `i386` and `armhf` architectures, data is collected from the [Reproducible Builds testing framework](https://tests.reproducible-builds.org/debian) is collected by this migration software even though, at the time of writing, it neither causes nor migration bonuses nor blocks migration. Indeed, the information only results are visible on Britney's [*excuses*](https://release.debian.org/britney/update_excuses.html) as well as on individual packages' pages on [*tracker.debian.org*](https://tracker.debian.org).

<br>

### Ubuntu Launchpad now supports `.buildinfo` files

Back in 2017, [Steve Langasek filed a bug](https://bugs.launchpad.net/launchpad/+bug/1686242) against Ubuntu's [Launchpad code hosting platform](https://launchpad.net) to report that `.changes` files (artifacts of building Ubuntu and Debian packages) reference `.buildinfo` files that aren't actually exposed by Launchpad itself. This was causing issues when attempting to process `.changes` files with tools such as [Lintian](https://lintian.debian.org). However, it was noticed last month that, in early August of this year, Simon Quigley had resolved this issue, and `.buildinfo` files are now available from the Launchpad system.

<br>

### PHP reproducibility updates

There have been two updates from the PHP programming language this month.

[![]({{ "/images/reports/2023-11/phpunit.png#right" | relative_url }})](https://phpunit.de/)

Firstly, the widely-deployed [PHPUnit](https://phpunit.de/) framework for the PHP programming language have recently released version 10.5.0, which introduces the inclusion of a `composer.lock` file, ensuring total reproducibility of the shipped binary file. Further details and the discussion that went into their particular implementation can be found on the [associated GitHub pull request](https://github.com/sebastianbergmann/phpunit/pull/5576).

In addition, the presentation "[*Leveraging Nix in the PHP ecosystem*](https://phpconference.com/web-development/leveraging-nix-php-ecosystem/)"  has been given in late October at the PHP International Conference in Munich by [Pol Dellaiera](https://github.com/drupol). While the video replay is not yet available, the (reproducible) [presentation slides and speaker notes](https://github.com/drupol/ipc2023/releases/tag/v23-79efbb4c24ab0d42c73906d16233a79d9659c5ca) are available.

<br>

### diffoscope changes

[![]({{ "/images/reports/2023-11/diffoscope.png#right" | relative_url }})](https://diffoscope.org/)

[diffoscope](https://diffoscope.org) is our in-depth and content-aware diff utility that can locate and diagnose reproducibility issues. This month, Chris Lamb made a number of changes, including:

* Improving DOS/MBR extraction by adding support for `7z`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/59b86c1f)]
* Adding a missing `RequiredToolNotFound` import.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/64ed5f38)]
* As a UI/UX improvement, try and avoid printing an extended traceback if *diffoscope* runs out of memory.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/bb887ddb)]
* Mark *diffoscope* as 'stable' on [PyPI.org](https://pypi.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e5e8d51e)]
* Uploading version `252` to Debian unstable.&nbsp;[[...](https://tracker.debian.org/news/1479028/accepted-diffoscope-252-source-into-unstable/)]

* Vagrant Cascadian updated diffoscope in GNU Guix to version [[252](https://issues.guix.gnu.org/67436)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=fbdcb31e13194c73db2b6578b2163fb194dc5002)].

<br>

### Website updates

[![]({{ "/images/reports/2023-11/website.png#right" | relative_url }})]({{ "/" | relative_url }})

A [huge number of notes]({{ "/events/hamburg2023/agenda/" | relative_url }}) were added to our website that were taken at our recent [Reproducible Builds Summit]({{ "/events/hamburg2023/" | relative_url }}) held between October 31st and November 2nd in Hamburg, Germany. In particular, a big thanks to Arnout Engelen, Bernhard M. Wiedemann, Daan De Meyer, Evangelos Ribeiro Tzaras, Holger Levsen and Orhun Parmaksız.

In addition to this, a number of other changes were made, including:

* Chris Lamb migrated the [website's homepage]({{ "/" | relative_url }}) to a ["hero" image](https://www.optimizely.com/optimization-glossary/hero-image/)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/2f50ba8a)], improved the [documentation related to `SOURCE_DATE_EPOCH` and CMake]({{ "/docs/source-date-epoch/#cmake" | relative_url }})&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/ee0d0e19)], added [iomart](https://www.iomart.com/) (neé Bytemark) and [DigitalOcean](https://www.digitalocean.com/) to [our sponsors page]({{ "/who/sponsors/" | relative_url }}")&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/16b73a33)] and dropped an unnecessary link on some horizontal navigation buttons&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/25cd328b)].

* Holger Levsen also made a [large number of notes pages]({{ "/events/venice2022/agenda/" | relative_url }}) from our [2022 summit in Venice]({{ "/events/venice2022/" | relative_url }}")&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/65072a36)], migrated the website's syntax highlighter from [Pygments]https://pygments.org/() to [Rouge](https://rouge.jneen.net/)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/5d46ea5d)], fixed some grammar on [our donate page]({{ "/donate/" | relative_url }})&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0343dfea)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/24bf9105)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/31b26b15)] and did a lot of updates to the Hamburg Summit's general information page &nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c8a86c6b)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/66691658)].

<br>


### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`amber-cli`](https://build.opensuse.org/request/show/1125191) (date-related issue)
    * [`bin86`](https://bugzilla.opensuse.org/show_bug.cgi?id=1217049) (FTBFS-2038)
    * [`buildah`](https://github.com/containers/buildah/issues/5191) (timestamp)
    * [`colord`](https://github.com/omgovich/colord/issues/122) (CPU)
    * [`google-noto-fonts`](https://build.opensuse.org/request/show/1127255) (file modification issue)
    * [`grub2`](https://bugzilla.opensuse.org/show_bug.cgi?id=1217619) (directory-related metadata)
    * [`guile-fibers`](https://build.opensuse.org/request/show/1127368) (parallelism issue)
    * [`guile-newt`](https://build.opensuse.org/request/show/1127367) (parallelism issue)
    * [`gutenprint`](https://build.opensuse.org/request/show/1128769) (embedded date/hostname)
    * [`hub`](https://github.com/mislav/hub/pull/3344) (random build path)
    * [`ipxe`](https://github.com/ipxe/ipxe/pull/1082) (nondeterministic behavoiour)
    * [`joker`](https://github.com/candid82/joker/pull/490) / [`joker`](https://github.com/candid82/joker/issues/491)
    * [`kopete`](https://invent.kde.org/network/kopete/-/merge_requests/14) (undefined behaviour)
    * [`kraft`](https://github.com/dragotin/kraft/pull/215) (embedde hostname)
    * [`libcamera`](https://bugzilla.opensuse.org/show_bug.cgi?id=1217690) (signature)
    * [`libguestfs`](https://bugzilla.opensuse.org/show_bug.cgi?id=1216986) (embeds build host file)
    * [`llvm`](https://github.com/llvm/llvm-project/issues/72206) (toolchain/Rust-related issue)
    * [`nfdump`](https://github.com/phaag/nfdump/pull/482) (date-related issue)
    * [`ovmf`](https://bugzilla.opensuse.org/show_bug.cgi?id=1217704) (unknown cause)
    * [`quazip`](https://build.opensuse.org/request/show/1129303) (missing fonts)
    * [`rdflib`](https://github.com/RDFLib/rdflib/issues/2645) (nondeterministic behaviour)
    * [`rpm`](https://github.com/rpm-software-management/rpm/pull/2762) (toolchain)
    * [`tigervnc`](https://build.opensuse.org/request/show/1129040) (embedded an RSA signature)
    * [`whatsie`](https://github.com/keshavbhatt/whatsie/pull/146) (date-related issue)
    * [`xen`](https://build.opensuse.org/request/show/1127661) (time-related issue)

* Cathy Hu:

    * [`policycoreutils`](https://github.com/SELinuxProject/selinux/commit/84e0884260c550ef840de6d09573444d93fb209a) (sort-related issue)

* Chris Lamb:

    * [#1055919](https://bugs.debian.org/1055919) filed against [`python-ansible-pygments`](https://tracker.debian.org/pkg/python-ansible-pygments).
    * [#1055920](https://bugs.debian.org/1055920) filed against [`bidict`](https://tracker.debian.org/pkg/bidict).
    * [#1056117](https://bugs.debian.org/1056117) filed against [`meson`](https://tracker.debian.org/pkg/meson).
    * [#1056118](https://bugs.debian.org/1056118) filed against [`radsecproxy`](https://tracker.debian.org/pkg/radsecproxy).
    * [#1056119](https://bugs.debian.org/1056119) filed against [`taffybar`](https://tracker.debian.org/pkg/taffybar).
    * [#1056398](https://bugs.debian.org/1056398) filed against [`php-doc`](https://tracker.debian.org/pkg/php-doc).
    * [#1056571](https://bugs.debian.org/1056571) filed against [`pelican`](https://tracker.debian.org/pkg/pelican).
    * [#1056572](https://bugs.debian.org/1056572) filed against [`maildir-utils`](https://tracker.debian.org/pkg/maildir-utils).
    * [#1056573](https://bugs.debian.org/1056573) filed against [`openmrac-data`](https://tracker.debian.org/pkg/openmrac-data).
    * [#1056649](https://bugs.debian.org/1056649) filed against [`vectorscan`](https://tracker.debian.org/pkg/vectorscan).

* Vagrant Cascadian:

    * [#1055969](https://bugs.debian.org/1055969) filed against [`kuttypy`](https://tracker.debian.org/pkg/kuttypy).

<br>

### Reproducibility testing framework

[![]({{ "/images/reports/2023-11/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework (available at [tests.reproducible-builds.org](https://tests.reproducible-builds.org)) in order to check packages and other artifacts for reproducibility. In November, a number of changes were made by Holger Levsen:

* Debian-related changes:

    * Track packages marked as `Priority: important` in a new package set.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c6f29664f)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f07341613)]
    * Stop scheduling packages that fail to build from source in *bookworm*&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8eccb4d0a)] and *bullseye*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/66d6790ba)].
    * Add old releases dashboard link in web navigation.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fec2cc86a)]
    * Permit re-run of the `pool_buildinfos` script to be re-run for a specific year.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e0a92972d)]
    * Grant *jbglaw* access to the `osuosl4` node&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bff966beb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ae18d6cd8)] along with *lynxis*&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f360bb8bb)].
    * Increase RAM on the `amd64` Ionos builders from 48 GiB to 64 GiB; thanks IONOS!&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/75526062d)]
    * Move *buster* to archived suites.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0766c7a6d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9f2a184a0)]
    * Reduce the number of `arm64` architecture workers from 24 to 16 in order to improve stability&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fc4e7eb44)], reduce the workers for `amd64` from 32 to 28 and, for `i386`, reduce from 12 down to 8&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/beca4dce8)].
    * Show the entire build history of each Debian package.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bd88512fd)]
    * Stop scheduling already tested package/version combinations in Debian *bookworm*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5045b6451)]

* [Snapshot service for rebuilders](https://rebuilder-snapshot.debian.net/)

    * Add an HTTP-based API endpoint.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0a7cd21a7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7c3c91b1a)]
    * Add a [Gunicorn](https://gunicorn.org/) instance to serve the HTTP API.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/990c992bf)]
    * Add an [NGINX](https://www.nginx.com/) config&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7fc0b85f8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d426d96d2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6bb41447b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0371c3e4f)]

* System-health:

    * Detect failures due to HTTP "503 Service Unavailable" errors.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dd1948b80)]
    * Detect failures to update package sets.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e672d5cf2)]
    * Detect unmet dependencies. (This usually occurs with builds of Debian `live-build`.)&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/af6de23d1)]

* Misc-related changes:

    * do install systemd-ommd on jenkins.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1f1d3828f)]
    * fix harmless typo in squid.conf for codethink04.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1f216cdcf)]
    * fixup: reproducible Debian: add gunicorn service to serve /api for rebuilder-snapshot.d.o.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9f99f7271)]
    * Increase codethink04's [Squid](http://www.squid-cache.org/) `cache_dir` size setting to 16 GiB.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1ee79e35e)]
    * Don't install `systemd-oomd` as it unfortunately kills `sshd`…&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8205cd4ef)]
    * Use `debootstrap` from backports when commisioning nodes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/458d30176)]
    * Add the `live_build_debian_stretch_gnome`, `debsums-tests_buster` and `debsums-tests_buster` jobs to the "zombie" list.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0610b3eff)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fcd5c2dea)]
    * Run `jekyll build` with the `--watch` argument when building the [Reproducible Builds website]({{ "/" | relative_url }}).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0ff00fef3)]
    * Misc node maintenance.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/19393c40c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5506b75d2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/655872a15)]

Other changes were made as well, however, including Mattia Rizzolo fixing `rc.local`'s Bash syntax so it can actually run&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4d593279c)], commenting away some file cleanup code that is (potentially) deleting too much&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/222b5302e)] and fixing the `html_brekages` page for Debian package builds&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ae1bb04fe)]. Finally, *наб* diagnosed and submitted a patch to add a `AddEncoding gzip .gz` line to the [*tests.reproducible-builds.org*](https://tests.reproducible-builds.org/) Apache configuration so that Gzip files aren't **re**-compressed as Gzip which some clients can't deal with (as well as being a waste of time).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/debcfa9ba)]

<br>

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)

 * Mastodon: [@reproducible_builds](https://fosstodon.org/@reproducible_builds)

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)
