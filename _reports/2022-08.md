---
layout: report
year: "2022"
month: "08"
title: "Reproducible Builds in August 2022"
draft: false
date: 2022-09-09 12:53:24
---

[![]({{ "/images/reports/2022-08/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the August 2022 report from the [Reproducible Builds](https://reproducible-builds.org) project!** In these reports we outline the most important things that we have been up to over the past month. As a quick recap, whilst anyone may inspect the source code of free software for malicious flaws, almost all software is distributed to end users as pre-compiled binaries. The motivation behind the reproducible builds effort is to ensure no flaws have been introduced during this compilation process by promising identical results are always generated from a given source, thus allowing multiple third-parties to come to a consensus on whether a build was compromised.

As ever, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

#### Community news

As announced last month, registration is currently **open** for our [in-person summit this year]({{ "/events/venice2022/" | relative_url }}) which is due to be held between **November 1st → November 3rd**. The event will take place in **Venice (Italy)**. Very soon we intend to pick a venue reachable via the train station and an international airport. However, the precise venue will depend on the number of attendees. Please see the [announcement email](https://lists.reproducible-builds.org/pipermail/rb-general/2022-July/002666.html) for information about how to register.

---

[![]({{ "/images/reports/2022-08/nsa.png#right" | relative_url }})](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/3146465/nsa-cisa-odni-release-software-supply-chain-guidance-for-developers/)

The US [National Security Agency](https://en.wikipedia.org/wiki/National_Security_Agency) (NSA), [Cybersecurity and Infrastructure Security Agency](https://en.wikipedia.org/wiki/Cybersecurity_and_Infrastructure_Security_Agency) (CISA) and the [Office of the Director of National Intelligence](https://en.wikipedia.org/wiki/Director_of_National_Intelligence#Office_of_the_Director_of_National_Intelligence) (ODNI) have released a document called "*Securing the Software Supply Chain: Recommended Practices Guide for Developers*" ([PDF](https://media.defense.gov/2022/Sep/01/2003068942/-1/-1/0/ESF_SECURING_THE_SOFTWARE_SUPPLY_CHAIN_DEVELOPERS.PDF)) as part of their Enduring Security Framework (ESF) work.

The document expressly recommends having reproducible builds as part of "advanced" recommended mitigations, along with hermetic builds. Page 31 (page 35 in the [PDF](https://media.defense.gov/2022/Sep/01/2003068942/-1/-1/0/ESF_SECURING_THE_SOFTWARE_SUPPLY_CHAIN_DEVELOPERS.PDF)) says:

> Reproducible builds provide additional protection and validation against attempts to compromise build systems. They ensure the binary products of each build system match: i.e., they are built from the same source, regardless of variable metadata such as the order of input files, timestamps, locales, and paths. Reproducible builds are those where re-running the build steps with identical input artifacts results in bit-for-bit identical output. Builds that cannot meet this must provide a justification why the build cannot be made reproducible.

The [full press release](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/3146465/nsa-cisa-odni-release-software-supply-chain-guidance-for-developers/) is available online.

---

[![]({{ "/images/reports/2022-08/appfair.png#right" | relative_url }})](https://appfair.net/)

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month, Marc Prud'hommeaux posted a feature request for *diffoscope* which additionally outlines a project called [The App Fair](https://appfair.net/),  an autonomous distribution network of free and open-source macOS and iOS applications, where "validated apps are then signed and submitted for publication".

---

Author/blogger [Cory Doctorow](https://craphound.com/bio/) posted published a provocative blog post this month titled "[Your computer is tormented by a wicked god](https://pluralistic.net/2022/07/28/descartes-was-an-optimist/#uh-oh)". Touching on Ken Thompson's famous talk, "[Reflections on Trusting Trust](https://www.cs.cmu.edu/~rdriley/487/papers/Thompson_1984_ReflectionsonTrustingTrust.pdf)", the early goals of "Secure Computing" and UEFI firmware interfaces:

> This is the core of a two-decade-old debate among security people, and it's one that the "benevolent God" faction has consistently had the upper hand in. They're the "curated computing" advocates who insist that preventing you from choosing an alternative app store or side-loading a program is for your own good – because if it's possible for you to override the manufacturer's wishes, then malicious software may impersonate you to do so, or you might be tricked into doing so. [..] This benevolent dictatorship model only works so long as the dictator is both perfectly benevolent and perfectly competent. We know the dictators aren't always benevolent. [...] But even if you trust a dictator's benevolence, you can't trust in their perfection. Everyone makes mistakes. Benevolent dictator computing works well, but fails badly. Designing a computer that intentionally can't be fully controlled by its owner is a nightmare, because that is a computer that, once compromised, can attack its owner with impunity.

---

Lastly, Chengyu HAN updated the [Reproducible Builds website]({{ "/" | relative_url }}) to correct an incorrect Git command.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/fc235bb9)]

#### Debian

[![]({{ "/images/reports/2022-08/debian.png#right" | relative_url }})](https://debian.org/)

In [Debian](https://debian.org/) this month, the `essential` and `required` package sets became 100% reproducible in Debian *bookworm* on the `amd64` and `arm64` architectures. These two subsets of the full Debian archive refer to Debian package "priority" levels as described in the [§2.5 Priorities](https://www.debian.org/doc/debian-policy/ch-archive.html#s-priorities) section of the [Debian Policy](https://www.debian.org/doc/debian-policy/) — there is no canonical "minimal installation" package set in Debian due to its diverse methods of installation.

As it happens, these package sets are *not* reproducible on the `i386` architecture because the `ncurses` package on that architecture is not yet reproducible, and the `sed` package currently fails to build from source on `armhf` too. The full list of reproducible packages within these package sets can be viewed within our QA system, such as on the page of [`required` packages in `amd64`](https://tests.reproducible-builds.org/debian/bookworm/amd64/pkg_set_required.html) and the list of [`essential` packages on `arm64`](https://tests.reproducible-builds.org/debian/bookworm/arm64/pkg_set_essential.html), both for Debian *bullseye*.

---

It recently has become very easy to install reproducible Debian Docker containers using `podman` on Debian bullseye:

```
$ sudo apt install podman
$ podman run --rm -it debian:bullseye bash
```

The (pre-built) image used is itself built using [*debuerrotype*](https://github.com/debuerreotype/debuerreotype), as explained on [*docker.debian.net*](https://docker.debian.net/). This page also details how to build the image yourself and what checksums are expected if you do so.

---

Related to this, it has also become straightforward to reproducibly bootstrap Debian using [*mmdebstrap*](https://gitlab.mister-muffin.de/josch/mmdebstrap), a replacement for the usual *debootstrap* tool to create Debian root filesystems:


```
$ SOURCE_DATE_EPOCH=$(date --utc --date=2022-08-29 +%s) mmdebstrap unstable > unstable.tar
```

This works for (at least) Debian *unstable*, *bullseye* and *bookworm*, and is tested automatically by a number of QA jobs set up by Holger Levsen ([*unstable*](https://jenkins.debian.net/job/reproducible_mmdebstrap_unstable/), [*bookworm*](https://jenkins.debian.net/job/reproducible_mmdebstrap_bookworm/) and [*bullseye*](https://jenkins.debian.net/job/reproducible_mmdebstrap_bullseye/))

---

Work has also taken place to ensure that the canonical *debootstrap* and *cdebootstrap* tools are *also* capable of bootstrapping Debian reproducibly, although it currently requires a few extra steps:

1. "Clamping" the modification time of files that are newer than `$SOURCE_DATE_EPOCH` to be not greater than `SOURCE_DATE_EPOCH`.

2. Deleting a few files. For *debootstrap*, this requires the deletion of `/etc/machine-id`, `/var/cache/ldconfig/aux-cache`, `/var/log/dpkg.log`, `/var/log/alternatives.log` and `/var/log/bootstrap.log`, and for *cdebootstrap* we also need to delete the `/var/log/apt/history.log` and `/var/log/apt/term.log` files as well.

This process works at least for *unstable*, *bullseye* and *bookworm* and is now being tested automatically by a number of QA jobs setup by Holger Levsen [[...](https://jenkins.debian.net/job/reproducible_debootstrap_bullseye/)][[...](https://jenkins.debian.net/job/reproducible_debootstrap_bookworm/)][[...](https://jenkins.debian.net/job/reproducible_debootstrap_unstable/)][[...](https://jenkins.debian.net/job/reproducible_cdebootstrap_bullseye/)][[...](https://jenkins.debian.net/job/reproducible_cdebootstrap_bookworm/)][[...](https://jenkins.debian.net/job/reproducible_cdebootstrap_unstable/)]. As part of this work, Holger filed two bugs to request a better initialisation of the `/etc/machine-id` file in both *debootstrap* [[...](https://bugs.debian.org/1018740)] and *cdebootstrap* [[...](https://bugs.debian.org/1018741)].

---

Elsewhere in Debian, 131 reviews of Debian packages were added, 20 were updated and 27 were removed this month, adding to [our extensive knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Chris Lamb added a number of issue types, including: `randomness_in_browserify_output` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/b2d75f42)],  `haskell_abi_hash_differences` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/732f0fc1)], `nondeterministic_ids_in_html_output_generated_by_python_sphinx_panels` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/3c9fbbcb)]. Lastly, Mattia Rizzolo removed the `deterministic` flag from the `captures_kernel_variant` flag [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/83ea690e)].

#### Other distributions

[![]({{ "/images/reports/2022-08/guix.png#right" | relative_url }})](https://guix.gnu.org/)

Vagrant Cascadian posted an [update of the status of Reproducible Builds in GNU Guix](https://lists.gnu.org/archive/html/guix-devel/2022-08/msg00161.html), writing that:

> `Ignoring the pesky unknown packages, it is more like ~93% reproducible`
> `and ~7% unreproducible... that feels a bit better to me!`
>
> `These numbers wander around over time, mostly due to packages moving`
> `back into an "unknown" state while the build farms catch up with each`
> `other... although the above numbers seem to have been pretty consistent`
> `over the last few days.`

The [post itself](https://lists.gnu.org/archive/html/guix-devel/2022-08/msg00161.html) contains a lot more details, including a brief discussion of tooling.

Elsewhere in GNU Guix, however, Vagrant updated a number of packages such as `itpp` [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=b30614b28cdc4eb893eeea4523109769f913499e)], `perl-class-methodmaker` [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=f31e55d0819064557b9a2af687f05b131f5c4f26)], `libnet` [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=d2b85f8906489fb95d8be41d6ba9fd520a5967d0)], `directfb` [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=397b103bef313c84eb662051fbcd28e223806bd3)] and `mm-common` [[...](https://issues.guix.gnu.org/57304)], as well as updated the version of *reprotest* to 0.7.21 [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=f030ae422b6d13a7a21158d9a37c9760597d1573)].

[![]({{ "/images/reports/2022-08/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In openSUSE, Bernhard M. Wiedemann published his usual [openSUSE monthly report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/KXF3OGYAJMGB5LU2QJJZSOCIAL22JUBU/).

#### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-08/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions `220` and `221` to Debian, as well as made the following changes:

* Update `external_tools.py` to reflect changes to `xxd` and the `vim-common` package. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c6432ec7)]
* Depend on the dedicated `xxd` package now, not the `vim-common` package. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8a823c69)]
* Don't crash if we can *open* a PDF file using the [PyPDF](ihttps://pybrary.net/pyPdf/) library, but cannot subsequently parse the annotations within. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/dbeab9e3)]

In addition, Vagrant Cascadian updated *diffoscope* in [GNU Guix](https://www.gnu.org/software/guix/), first to to version 220 [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=04ef952a4928a427fa3d778e23d4e99299c9fa5a)] and later to 221 [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=ec6122250de7c83a7e77054584a34767b11337db)].

#### Community news

The Reproducible Builds project aims to fix as many currently-unreproducible packages as possible as well as to send all of our patches upstream wherever appropriate. This month we created a number of patches, including:

* Bernhard M. Wiedemann:

    * [`at-spi-sharp`](https://build.opensuse.org/request/show/1000142) (build failure when build on a multiprocessor machine).
    * [`borgbackup`](https://github.com/borgbackup/borg/issues/6996) (fails to build in 2038, [fix](https://build.opensuse.org/request/show/999784))
    * [`buzztrax`](https://build.opensuse.org/request/show/1000483) (parallelism-related issue)
    * [`chart-testing`](https://build.opensuse.org/request/show/993689) (date-related issue)
    * [`memcached`](https://github.com/memcached/memcached/pull/927) (fails to build in 2038)
    * [`nim`](https://github.com/nim-lang/Nim/issues/20285) (fails to build in 2038)
    * [`perl-Time-Moment`](https://github.com/chansen/p5-time-moment/pull/48) (fails to build in 2038)
    * [`python-bson`](https://github.com/py-bson/bson/pull/117) (fails to build in 2038)
    * [`python-heatclient`](https://review.opendev.org/c/openstack/python-heatclient/+/855083) (fails to build in 2038)
    * [`python3.8`](https://bugzilla.opensuse.org/show_bug.cgi?id=1202666) (fails to build in 2038)
    * [`reproducible-faketools`](https://build.opensuse.org/request/show/992182)
    * [`s3fs`](https://github.com/s3fs-fuse/s3fs-fuse/pull/2026) (date-related issue)
    * [`systemd`](https://build.opensuse.org/request/show/994525) (date-related issue)

* Chris Lamb:

    * [#1016486](https://bugs.debian.org/1016486) filed against [`wayfire`](https://tracker.debian.org/pkg/wayfire).
    * [#1016583](https://bugs.debian.org/1016583) filed against [`multipath-tools`](https://tracker.debian.org/pkg/multipath-tools).
    * [#1016584](https://bugs.debian.org/1016584) filed against [`node-canvas-confetti`](https://tracker.debian.org/pkg/node-canvas-confetti).
    * [#1017473](https://bugs.debian.org/1017473) filed against [`psi`](https://tracker.debian.org/pkg/psi) ([forwarded upstream](https://github.com/psi-im/psi/pull/699)).
    * [#1017475](https://bugs.debian.org/1017475) filed against [`sphinx-panels`](https://tracker.debian.org/pkg/sphinx-panels) ([forwarded upstream](https://github.com/executablebooks/sphinx-panels/pull/82)).
    * [#1017920](https://bugs.debian.org/1017920) filed against [`sysfsutils`](https://tracker.debian.org/pkg/sysfsutils).
    * [#1017945](https://bugs.debian.org/1017945) filed against [`geeqie`](https://tracker.debian.org/pkg/geeqie).

* Vagrant Cascadian:

    * [#1017073](https://bugs.debian.org/1017073) filed against [`python-suntime`](https://tracker.debian.org/pkg/python-suntime).
    * [#1017372](https://bugs.debian.org/1017372) filed against [`plymouth`](https://tracker.debian.org/pkg/plymouth).
    * [#1017373](https://bugs.debian.org/1017373) filed against [`tiemu`](https://tracker.debian.org/pkg/tiemu).
    * [#1017421](https://bugs.debian.org/1017421) filed against [`fltk1.3`](https://tracker.debian.org/pkg/fltk1.3).
    * [#1018802](https://bugs.debian.org/1018802) filed against [`localechooser`](https://tracker.debian.org/pkg/localechooser).
    * [`uboot`](https://lists.denx.de/pipermail/u-boot/2022-August/492156.html) (Rasmus Villemoes proposed fixing gcc instead [[...](https://gcc.gnu.org/pipermail/gcc-patches/2022-August/600491.html)][[...](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=93371)])

#### Testing framework

[![]({{ "/images/reports/2022-08/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, Holger Levsen made the following changes:

* Debian-related changes:

    * Temporarily add Debian *unstable* `deb-src` lines to enable test builds a [Non-maintainer Upload](https://wiki.debian.org/NonMaintainerUpload) (NMU) campaign targeting 708 sources without `.buildinfo` files found in Debian *unstable*, including 475 in bookworm. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/87cf75b0)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/2ddfa8e1)]
    * Correctly deal with the [Debian Edu](https://wiki.debian.org/DebianEdu/) packages not being installable. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/c6503196)]
    * Finally, stop scheduling *stretch*. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/40c48f0b)]
    * Make sure all Ubuntu nodes have the `linux-image-generic` kernel package installed. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/61f1c057)]

* Health checks & view:

    * Detect [SSH](https://www.openssh.com/) login problems. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/6a98f816)]
    * Only report the first uninstallable package set. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/c69cb969)]
    * Show new bootstrap jobs. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/2d64b3ee)] and *debian-live* jobs. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/73cc19d3)] in the job health view.
    * Fix regular expression to detect various zombie jobs. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/c42d00ff)]

* New jobs:

    * Add a new job to test reproducibility of [*mmdebstrap*](https://gitlab.mister-muffin.de/josch/mmdebstrap) bootstrapping tool. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/1a4bf7a5)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/2ecb9c31)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/79810a93)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/e2885c3e)]
    * Run our new *mmdebstrap* job remotely [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/088d6d77)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/2f11e8cf)]
    * Improve the output of the *mmdebstrap* job. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/b40cee9e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/41612318)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/1333cb55)]
    * Adjust the *mmdebstrap* script to additionally support *debootstrap* as well. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/6294d6df)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/297b9827)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/bf292af4)]
    * Work around *mmdebstrap* and *debootstrap* keeping logfiles within their artifacts. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/955cc9ba)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/06f47311)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/4bdda15e)]
    * Add support for testing *cdebootstrap* too and add such a job for unstable. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/c1bd3041)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/f71341bb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/835bdc62)]
    * Use a reproducible value for `SOURCE_DATE_EPOCH` for all our new bootstrap jobs. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/262045c5)]

* Misc changes:

    * Send the `create_meta_pkg_sets` notification to `#debian-reproducible-changes` instead of `#debian-reproducible`. [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/afbd1d10)]

In addition, Roland Clobus re-enabled the tests for *live-build* images [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/e4d7f0b5)] and added a feature where the build would retry instead of give up when the archive was synced whilst building an ISO [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/7b28da62)], and Vagrant Cascadian added logging to report the current target of the `/bin/sh` symlink [[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/e4776858)].

#### Contact

As ever, if you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
