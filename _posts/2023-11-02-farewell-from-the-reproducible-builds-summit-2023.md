---
layout: post
title:  "Farewell from the Reproducible Builds Summit 2023!"
date: 2023-11-02
categories: org
---

Farewell from the *Reproducible Builds* summit, which just took place in **Hamburg, Germany**:
{: .lead}

[![]({{ "/images/news/2023-11-02-farewell-from-the-reproducible-builds-summit-2023/group_photo.jpg" | relative_url }})]({{ "/events/hamburg2023/" | relative_url }})

This year, we were thrilled to host the seventh edition of this exciting event. Topics covered this year included:

* Project updates from openSUSE, Fedora, Debian, ElectroBSD, Reproducible Central and NixOS
* Mapping the "big picture"
* Towards a snapshot service
* Understanding user-facing needs and personas
* Language-specific package managers
* Defining our definitions
* Creating a "Ten Commandments" of reproducibility
* Embedded systems
* Next steps in GNU Guix' reproducibility
* Signature storage and sharing
* Public verification services
* Verification use cases
* Web site audiences
* Enabling new projects to be "born reproducible"
* Collecting reproducibility success stories
* Reproducibility's relationship to SBOMs
* SBOMs for RPM-based distributions
* Filtering diffoscope output
* Reproducibility of filesystem images, filesystems and containers
* Using verification data
* A deep-dive on Fedora and Arch Linux package reproducibility
* Debian rebuild archive service discussion

… as well as countless informal discussions and hacking sessions into the night. Projects represented at the venue included:

<div class="text-center text-muted">
Debian, openSUSE, QubesOS, GNU Guix, Arch Linux, phosh, Mobian, PureOS, JustBuild, LibreOffice, Warpforge, OpenWrt, F-Droid, NixOS, ElectroBSD, Apache Security, Buildroot, Systemd, Apache Maven, Fedora, Privoxy, CHAINS (KTH Royal Institute of Technology), coreboot, GitHub, Tor Project, Ubuntu, rebuilderd, repro-env, spytrap-adb, arch-repro-status, etc.
</div>

---

A huge thanks to our sponsors and partners for making the event possible:

<div class="row p-md-4 p-sm-2 pt-5 pb-5">
    <div class="col-xs-12 col-sm-6 mb-6 pb-3 mx-auto">
        <div class="card text-center">
            <a href="https://aspirationtech.org/">
                <img class="px-5 pt-5 pb-4 project-img" src="{{ "/images/logos/aspirationtech.jpg" | relative_url }}" alt="Aspiration">
            </a>
            <p class="text-muted mb-4">Event facilitation</p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 mb-6 pb-3 mx-auto">
        <div class="card text-center">
            <a href="https://mullvad.net/" name="Mullvad">
                <img class="px-5 pt-5 pb-4 project-img" src="/assets/images/sponsors/mullvad.svg" alt="Mullvad">
            </a>
            <p class="text-muted mb-4">Platinum sponsor</p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 mb-6 pb-3 mx-auto">
        <div class="card text-center">
            <a href="https://www.opensuse.org/" name="openSUSE">
                <img class="px-5 pt-5 project-img" src="/images/logos/openSUSE.png" alt="openSUSE">
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 mb-6 pb-3 mx-auto">
        <div class="card text-center">
            <a href="https://www.debian.org/" name="Debian">
                <img class="p-5 project-img" src="/images/logos/debian.png" alt="Debian">
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 mb-6 pb-3 mx-auto">
        <div class="card text-center">
            <a href="https://sfconservancy.org/" name="Software Freedom Conservancy">
                <img class="p-5 project-img" src="/images/logos/sfconservancy.svg" alt="Software Freedom Conservancy">
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 mb-6 pb-3 mx-auto">
        <div class="card text-center">
            <a href="https://www.debian.org/" name="allotropia software GmbH">
                <img class="p-5 project-img" src="/assets/images/sponsors/allotropia.svg" alt="allotropia software GmbH">
            </a>
        </div>
    </div>
</div>

<br>

If you weren't able to make it this year, don't worry; just look out for an announcement in 2024 for the next event.
