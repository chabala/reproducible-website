---
layout: post
title: "Supporter spotlight: Amateur Radio Digital Communications (ARDC)"
date: 2022-04-14 10:00:00
categories: org
---

[![]({{ "/images/news/supporter-spotlight-ardc/ardc.png?2#right" | relative_url }})](https://www.ampr.org/)

<big>The Reproducible Builds project relies on [several projects, supporters
and sponsors]({{ "/who/" | relative_url }}) for financial support, but they are
also valued as ambassadors who spread the word about the project and the work
that we do.</big>

This is the third instalment in a series featuring the projects, companies
and individuals who support the Reproducible Builds project. If you are a
supporter of the Reproducible Builds project (of whatever size) and would like
to be featured here, please let get in touch with us at
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).

We started this series by
[featuring the Civil Infrastructure Platform]({{ "/news/2020/10/21/supporter-spotlight-cip-project/" | relative_url }})
project and followed this up with a
[post about the Ford Foundation]({{ "/news/2021/04/06/supporter-spotlight-ford-foundation/" | relative_url }}). Today,
however, we'll be talking with **Dan Romanchik**, Communications Manager at
[**Amateur Radio Digital Communications** (ARDC)](https://www.ampr.org/).

<br>

**Chris Lamb: Hey Dan, it's nice to meet you! So, for someone who has not
heard of Amateur Radio Digital Communications (ARDC) before, could you tell
us what your foundation is about?**

Dan: Sure! ARDC's mission is to support, promote, and enhance experimentation,
education, development, open access, and innovation in amateur radio, digital
communication, and information and communication science and technology. We
fulfill that mission in two ways:

1. We administer an allocation of IP addresses that we call 44Net. These IP
addresses (in the *44.0.0.0/8* IP range) can only be used for amateur radio
applications and experimentation.

2. We make grants to organizations whose work aligns with our mission. This
includes amateur radio clubs as well as other amateur radio-related
organizations and activities. Additionally, we support scholarship programs for
people who either have an amateur radio license or are pursuing careers in
technology, [STEM education](https://en.wikipedia.org/wiki/Science,_technology,_engineering,_and_mathematics)
and open-source software development projects that fit our mission, such as
Reproducible Builds.

<br>

[![]({{ "/images/news/supporter-spotlight-ardc/Amateurfunkstation.jpg#right" | relative_url }})](https://en.wikipedia.org/wiki/Amateur_radio)

**Chris: How might you relate the importance of amateur radio and similar
technologies to someone who is non-technical?**

Dan: Amateur radio is important in a number of ways. First of all, amateur
radio is a **public service**. In fact, the legal name for amateur radio is the
Amateur Radio Service, and one of the primary reasons that amateur radio
exists is to provide emergency and public service communications. All over the
world, amateur radio operators are prepared to step up and provide emergency
communications when disaster strikes or to provide communications for events
such as marathons or bicycle tours.

Second, amateur radio is important because it helps **advance the state of the
art**. By experimenting with different circuits and communications techniques,
amateurs have made significant contributions to communications science and
technology.

Third, amateur radio plays a big part in **technical education**. It enables
students to experiment with wireless technologies and electronics in ways that
aren't possible without a license. Amateur radio has historically been a
gateway for young people interested in pursuing a career in engineering or
science, such as network or electrical engineering.

Fourth — and this point is a little less obvious than the first three — amateur
radio is a way to enhance *international goodwill and community*. Radio knows
no boundaries, of course, and amateurs are therefore ambassadors for their
country, reaching out to all around the world.

Beyond amateur radio, ARDC also supports and promotes research and innovation
in the broader field of digital communication and information and communication
science and technology. Information and communication technology plays a big
part in our lives, be it for business, education, or personal communications.
For example, think of the impact that cell phones have had on our culture. The
challenge is that much of this work is proprietary and owned by large
corporations. By focusing on open source work in this area, we help open the
door to innovation outside of the corporate landscape, which is important to
overall technological resiliency.

<br>

**Chris: Could you briefly outline the history of ARDC?**

[![]({{ "/images/news/supporter-spotlight-ardc/iana.png#right" | relative_url }})](https://www.iana.org/)

Dan: Nearly forty years ago, a group of visionary 'hams' saw the future
possibilities of what was to become the internet and requested an address
allocation from the
[Internet Assigned Numbers Authority](https://www.iana.org/) (IANA). That
allocation included more than sixteen million IPv4 addresses, **44.0.0.0**
through **44.255.255.255**. These addresses have been used exclusively for
amateur radio applications and experimentation with digital communications
techniques ever since. In 2011, the informal group of hams administering these
addresses incorporated as a nonprofit corporation, Amateur Radio Digital
Communications (ARDC). ARDC is recognized by IANA,
[ARIN](https://www.arin.net/) and the other Internet Registries as the sole
owner of these addresses, which are also known as [AMPRNet](https://en.wikipedia.org/wiki/AMPRNet)
or 44Net.

Over the years, ARDC has assigned addresses to thousands of hams on a long-term
loan (essentially acting as a zero-cost lease), allowing them to experiment
with digital communications technology. Using these IP addresses, hams have
carried out some very interesting and worthwhile research projects and
developed practical applications, including TCP/IP connectivity via radio
links, digital voice, telemetry and repeater linking.

Even so, the amateur radio community never used much more than half the
available addresses, and today, less than one third of the address space is
assigned and in use. This is one of the reasons that ARDC, in 2019, decided to
sell one quarter of the address space (or approximately 4 million IP addresses)
and establish an endowment with the proceeds. This endowment now funds ARDC's a
suite of grants, including scholarships, research projects, and of course
amateur radio projects. Initially, ARDC was restricted to awarding grants to
organizations in the United States, but is now able to provide funds to
organizations around the world.

<br>

**Chris: How does the Reproducible Builds effort help ARDC achieve its goals?**

Dan: Our aspirational goals include:

* Broad reach. We look for projects that will benefit the widest community
  possible.

* Social over commercial benefit. We prioritize giving to organizations and
  funding projects that may not have a viable business model, but provide a
  clear benefit to society.

* Preservation of the right to innovate. We oppose any efforts that restrict
  innovation to protect the status quo and keep the current winners in
  privileged positions.

We think that the Reproducible Builds' efforts in helping to ensure the safety
and security of open source software closely align with those goals.

<br>

**Chris: Are there any specific 'success stories' that ARDC is particularly
proud of?**

[![]({{ "/images/news/supporter-spotlight-ardc/hoopa-nsn.png#right" | relative_url }})](https://www.hoopa-nsn.gov/)

Dan: We are really proud of our grant to the [Hoopa Valley Tribe](https://www.hoopa-nsn.gov/)
in California. With a population of nearly 2,100, their reservation is the
largest in California.  Like everywhere else, the COVID-19 pandemic hit the
reservation hard, and the lack of broadband internet access meant that 130
children on the reservation were unable to attend school remotely.

The ARDC grant allowed the tribe to address the immediate broadband needs in
the Hoopa Valley, as well as encourage the use of amateur radio and other
two-way communications on the reservation. The first nation was able to deploy
a network that provides broadband access to approximately 90% of the residents
in the valley. And, in addition to bringing remote education to those 130
children, the Hoopa now use the network for remote medical monitoring and
consultation, adult education, and other applications.

Other successes include our grants to:

* The [ARRL Foundation](http://www.arrl.org/the-arrl-foundation), which has
  awarded dozens of scholarships over the past couple of years to amateur radio
  operators both young and old.

[![]({{ "/images/news/supporter-spotlight-ardc/cvarc.png#right" | relative_url }})](https://w9cva.org/)

* The [Chippewa Valley Amateur Club](https://w9cva.org/), who used the grant to
  build an emergency communications trailer and improve the emergency
  communication infrastructure in their community.

* [Woodridge Middle School](https://www.ampr.org/grants-operation-hamulanche/),
  who have developed innovative, hands-on, radio-related projects that have
  dramatically increased the test scores of the kids in their STEM program.

* The [Make Operating Radio Easier (MORE) Project](https://www.ampr.org/grant-make-operating-radio-easier-more/),
  which aims to reduce both gender and age imbalances in ham radio, through
  education and hands-on activities. Led by the IEEE Central Jersey Section,
  MORE provides mentoring, proactive intervention, and inclusivity to groups
  that are under-represented in amateur radio.

<br>

**Chris: ARDC supports a number of other existing projects and initiatives, not
all of them in the open source world. How much do you feel being a  part of the
broader 'free culture' movement helps you achieve your aims?**

Dan: In general, we find it challenging that most digital communications technology
is proprietary and closed-source. It's part of our mission to fund open source
alternatives. Without them, we are solely reliant, as a society, on corporate
interests for our digital communication needs. It makes us vulnerable and it
puts us at risk of increased surveillance. Thus, ARDC supports open source
software wherever possible, and our grantees must make a commitment to share
their work under an open source license or otherwise make it as freely
available as possible.

<br>

**Chris: Thanks so much for taking the time to talk to us today. Now, if
someone wanted to know more about ARDC or to get involved, where might they
go to look?**

[![]({{ "/images/news/supporter-spotlight-ardc/ardc-sm.png?2#right" | relative_url }})](https://www.ampr.org/)

To learn more about ARDC in general, please visit our website at
[https://www.ampr.org](https://www.ampr.org).

To learn more about 44Net, go to
[https://wiki.ampr.org/wiki/Main_Page](https://wiki.ampr.org/wiki/Main_Page).

And, finally, to learn more about our grants program, go to
[https://www.ampr.org/apply/](https://www.ampr.org/apply/)

<br>

---

*For more about the Reproducible Builds project, please see our website at
[reproducible-builds.org]({{ "/" | relative_url }}). If you are interested in
ensuring the ongoing security of the software that underpins our civilisation
and wish to sponsor the Reproducible Builds project, please reach out to the
project by emailing
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).*
