---
layout: no_sidebar
title: Home
title_head: Reproducible Builds &mdash; a set of software development practices that create an independently-verifiable path from source to binary code
order: 10
permalink: /
---

<div class="hero mb-4">
    <div class="container text-center">
        <div class="col-sm-8 offset-sm-2">
            <a href="{{ "/" | relative_url }}">
                <img class="mb-4 img-fluid" src="{{ "/assets/images/logo-text-white.png" | relative_url }}" alt="Reproducible Builds" />
            </a>

            <p class="lead mt-5 pt-5 pb-5">
                <strong>Reproducible builds</strong> are a set of software development
                practices that create an independently-verifiable path from source
                to binary&nbsp;code.

                <small class="d-none d-sm-inline">
                    (<a href="{{ "/docs/definition/" | relative_url }}">more</a>)
                </small>
            </p>
        </div>
    </div>
</div>

{% include nav_buttons.html %}

## Why does it matter?

Whilst anyone may inspect the source code of free and open source software for
malicious flaws, most software is distributed pre-compiled with no method to
confirm whether they correspond.

This incentivises attacks on developers who release software, not only via
traditional exploitation, but also in the forms of political influence,
blackmail or even threats of violence.

This is particularly a concern for developers collaborating on privacy or
security software: attacking these typically result in compromising
particularly politically-sensitive targets such as dissidents, journalists and
whistleblowers, as well as anyone wishing to communicate securely under a
repressive regime.

Whilst individual developers are a natural target, it additionally encourages
attacks on build infrastructure as a successful attack would provide access to
a large number of downstream computer systems. By modifying the generated
binaries here instead of modifying the upstream source code, illicit changes
are essentially invisible to its original authors and users alike.

The motivation behind the **Reproducible Builds** project is therefore to allow
verification that no vulnerabilities or backdoors have been introduced during
this compilation process. By promising identical results are always
generated from a given source, this allows multiple third parties to come
to a consensus on a "correct" result, highlighting any deviations as suspect
and worthy of scrutiny.

This ability to notice if a developer or build system has been compromised
then prevents such threats or attacks occurring in the first place, as any
compromise can be quickly detected. As a result, front-liners cannot be
threatened/coerced into exploiting or exposing their colleagues.

[Several free software projects]({{ "/who/" | relative_url }})
already, or will soon, provide reproducible builds.

## How?

First, the **build system** needs to be made entirely deterministic:
transforming a given source must always create the same result. For example,
the current date and time must not be recorded and output always has to be
written in the same order.

Second, the set of tools used to perform the build and more generally the
**build environment** should either be recorded or pre-defined.

Third, users should be given a way to recreate a close enough build
environment, perform the build process, and **validate** that the output matches
the original build.

Learn more about [how to make your software build reproducibly…]({{ "/docs" | relative_url }})

## Recent monthly reports

<ul class="list-unstyled">
    {% assign reports = site.reports | sort: 'year, month' | where: 'draft', 'false' | reverse %}
    {% for x in reports limit: 3 %}
    <li>
        <span class="text-muted">{{ x.date | date: "%b %-d, %Y" }}</span>:
        <a href="{{ x.url | relative_url }}">{{ x.title }}</a>
    </li>
    {% endfor %}
</ul>

([See all reports…]({{ "/news/" | relative_url }}))

## Recent news

<ul class="list-unstyled">
    {% assign posts = site.posts | where: 'draft', 'false' %}
    {% for x in posts limit: 3 %}
    <li>
        <span class="text-muted">{{ x.date | date: "%b %-d, %Y" }}</span>:
        <a href="{{ x.url | relative_url }}">{{ x.title }}</a>
    </li>
    {% endfor %}
</ul>

([See all…]({{ "/news/" | relative_url }}))


{% assign sponsors = site.data.sponsors.platinum | sort: 'name' %}
{% if sponsors.size != 0 %}
## Sponsors

We are proud to be [sponsored by]({{ "/sponsors/" | relative_url }}):

<div class="row bg-light p-md-4 p-sm-2 pt-5 pb-5">
    {% for x in sponsors %}
    <div class="col-xs-12 col-sm-6 mb-6 mx-auto">
        <div class="card text-center h-100">
            <a href="{{ x.url }}" name="{{ x.name }}">
                <img class="p-5 project-img sponsor-img-platinum" src="{{ x.logo | prepend: "/assets/images/sponsors/" | relative_url }}" alt="{{ x.name }}">
            </a>
        </div>
    </div>
    {% endfor %}
</div>
{% else %}
{% endif %}

<br>

{% include nav_buttons.html %}
