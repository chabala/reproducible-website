---
layout: event_detail
title: Collaborative Working Sessions - Public verification service
event: hamburg2023
order: 205
permalink: /events/hamburg2023/verification1/
---

# Server collects build data

- Includes Hashes of Outputs
- Info About Build Environment
- Finds out what environment factors matter

# Use cases

## Use data to determine what's causing builds to differ
## What percentage of X builds reproducibly



# Building or rebuilding stuff

Components are things like build environment and sources

## Build spec

Build spec:
 - Input archive
 - Patches
 - Build instructions
 - Target distro/OS


Environment:
 - What's installed
 - Contents of /etc
 - File system types
 - Initial working directory
 - Environment variables
   - TZ
   - Locale
 - Running kernel
 - Hardware architecture
 - Current user (UID/GID)

Outputs:
 - 'treeish' hash
 - Include some file metadata, but not all
 - Should timestamps be stored?
 - Is-Test (delete periodically if true)

(above is the payload)


Metadata:
 - Name + Version
 - Project URL
 - Uploader
 - Optional signature
 - Comment
 - Link to build
 
 Formats:
     - Linked Data / RDF
     - JSON
     - SBOM / SPDX / CycloneDX / ... ?
     - Maybe In-TOTO?
     
Hook In:
    - After 'Fetch' / Before 'Build'
    - After 'Artifact Generation'


People interested in contributing to implementation:
    - Hervé Boutemy (hboutemy@apache.org)
    - Arnout Engelen (arnout@bzzt.net)
    - Janis Peyer (janispeyer@bluewin.ch)
    - Nicolas (boklm@torptoject.org)
    - quae@daurnimator.com

