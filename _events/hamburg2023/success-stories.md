---
layout: event_detail
title: Mapping the Big Picture - Success Stories
event: hamburg2023
order: 24
permalink: /events/hamburg2023/success-stories/
---

## Real world success stories: What we know works
- clear cases
  - `SOURCE_DATE_EPOCH` *widely* honored and standardized
  - ElectroBSD (distribution tar balls amd64)
  - near 500 Java project produce RB releases (see Reproducible Central)
  - Yocto base
  - NixOS minimal installation ISO reproducible
  - Tails ISO
  - Tor Browser is reproducible
  - Debian docker images
  - diverse double compilation to encounter Trusting-trust attacs
  - Debian policy
  - bitcoin core
  - find+fix corruption bugs
- nobody knows (missing docs etc)
  - zig
  - go toolchain?
  - android
  - [Spoon](https://github.com/INRIA/spoon): An AST parsing and transformation library for Java
  - openSUSE at 97%
  - GitHub reproroducible build badge
  - spytrap-adb
  - F-Droid 90% new apps included are reproducible
  - bootstrappable.org
  - coreboot
  - developer stories

## Real world success stories we need or are searching for

(RB Success Stories Desired)

## column 1: < 100%

* 96% binary pkg reproducible debian cloud image
* K && N trust in GNU Guix substitues by default
* Debug Packages
* Debian Install Images reproducible

## column 2

* 150 java projects try fail at getting RB release (really hard to read) J
* f-droid: older apps RB verify but not yet switched
* only release reproducible packages (stage until verified)

## column 3

* Reproducible compilers built with other compilers. e.g. gcc built via clang then rebuild gcc with that
* setting up CI RB system for FEdora RPMs
* Install images rebuilt on different distro
* Install images rebuilt by different organisations
* Repo reproducible as opposed to deterministic build
* Debian snapshot scalable


