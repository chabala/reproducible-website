---
layout: event_detail
title: Collaborative Working Sessions - Language-specific package managers
event: hamburg2023
order: 32
permalink: /events/hamburg2023/language-specific/
---

* Packaging, source or binary? Python and Rust (crates) supports both
* Crates have immutable tags, git usually does not
* Source provance is important but usually hard to get
* Score card can help
* GOSST (Google) scans packages and try to rebuild it's _content_ with
  some success, results are not yet published
    * If results were published, could be used to add badges to the
      packages in the repositories that the package was rebuilt and
      verified by a third party builder
    * Could be used for cli integration to only allow install of
      packages being rebuilt/verified by a third party
    * Compare here is the binary/source artifact, not all metadata
    * Makes it easier to adopt as maintainers do not have to change
      all their CI/CD workflows
* Discussed hosted vs local builder and trustworthyness, if the buid
  is being reproduced, both hosted and local builder can be trusted
    * Having developers managing key materials can still be hard
* Can we tie Scorecard data into the package registry?
* Can we have workflows that triggers a rebuild on a release, and gate
  the publish step with a verified rebuild?
* First action point:
    * Have thirdparty builders rebuilt packages similar to what Herve
      is doing for Maven Central?



