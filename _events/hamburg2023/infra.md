---
layout: event_detail
title: Mapping the Big Picture - Mapping projects infra
event: hamburg2023
order: 26
permalink: /events/hamburg2023/infra/
---

* Projects practicing reproducibility
  * Arch Linux
  * Distrust Toolchain
  * Buildroot
  * Coreboot
  * GNU Guix
  * NixOS
  * Warpforge
  * ElectroBSD
  * OpenWRT
  * Fedora Linux
  * openSUSE
  * F-Droid
  * Java jar Archives
  * Debian
  * TOR
  * TAILS
  * so toolchain
  * Apache Maven
  * Qubes OS
  * Scala+sbt
* What projects/platforms/libraries do we *want*/need to be reproducible?
  * Maven (artifacts without sources)
  * NPM
  * Rust crates
  * PyPI
  * Docker Directory Timestamps
  * ElectroBSD, FreeBSD Ports/Packages
  * Binutils
  * Qt
  * Python sphinx
  * ar (static .a libraries)
  * Gradle
  * DPkg database
  * PureOS/Mobian
  * R-B in Ubuntu
  * Compilers for embedded systems
    * Aurix (Infineon)
    * ARM/KEIL
	* GCC (its own build and the binaries it builds)
	* Python 3.9
	* Filesystems
	* Clojure
      * Build Tools
      * Lein
	* Flatpack
	* Docker Images
* Missing RB infrastructure we need to create
  * [The N commandments of reproducible builds]({{ "/events/hamburg2023/rb-commandments/" | relative_url }})
  * Alpine pkg archive
  * Recordin Diversity
  * Debian debuginfod.debian.net should also provide sources (stripped paths seems to be part of the problem)
  * Rebuild infra for custom projects
  * Standard for build location like we have for SOURCE_DATE_EPOCH for time
  * Debian Snapshot server
  * PyPI Repository
  * Firmware hashes
  * Cross target reproducibility
  * Guix QA testing reproducibility
  * Shared tooling for reporting
  * Sharing + reporting on build results
  * Comparing reproducibility stats Guix packages with other projects
  * Crowd sourced reproducibility status information
  * Diffoscope but I can install it (fewer deps)
  * Merkle tree of upstream source releases
  * Reproducible vs PGO
  * Build infrastructure for ElectroBSD
  * Standard BUILDINFO file format
  * GCC 4.7.4 bootstrapped on more arches for bootstrappable builds

https://salsa.debian.org/reproducible-builds/reproducible-website/-/merge_requests/105
