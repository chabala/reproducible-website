---
layout: event_detail
title: Mapping the Big Picture - Mapping lists
event: hamburg2023
order: 27
permalink: /events/hamburg2023/lists/
---

* Problems we need to discuss/solve
  * NetBSD: LTO test in tests.tgz
  * NetBSD: "-O bigdir" breaks it completely :(
  * Reproducible Filesystem images
  * Users caring
  * Easy to use/understand tools for end-users
  * Generated sources from graphic presentation
  * comparability of graphic sources
  * snapshots.debian.org for OpenWRT package source code
  * Geting all sources eg. `go mod download`
  * cross-compilation (even a different CPU feature is)
  * Further improving knowledge sharing between distros
  * Rproduce & challenge
  * Diffoscope to (opt-in) ignore embedded signatures
  * missing archive of build dependencies
  * Github/Gitlab patches (git hash abbreviation) changing in length
  * "Provenance" (Don't embed!)
  * Build Artifact Retention
  * Avoid fragmentation in signing formats PGP  JWT  WAC VC  ZWK  SIGSTORE   NOTARY
  * Reproducible profile-guided-optimization
  * Undocumented build environmets
* Other topics we need to discuss
  * t-shirts and other swag
  * Binary tarnsparency
  * Too much dependencies...?
  * Bootstrappability
  * What work can be Distro-Agnostic?
  * Potential blog posts for reproducible-builds.org
  * diffoscope improvements
  * index of binaries of a distro, keyed by hash (i.e. map binary -> src)
* Other lists we need to make
  * "sister" projects of reproducibility
    * SLSA
    * Bootstrappable Builds
  * R-B hackathon organize
  * List of our RB-related tooling
  * List of Existing RB Infrastructure
  * List of Reasons for investing in R-B
    * Similar to buy-in page
* [Other projects/people we should get to the next Summit]({{ "/events/hamburg2023/projects/" | relative_url }})

# Other Projects / People to invite to next summit

* Nuget Gallery
* Language Package managers (all of them!)
* Language registries (repositories) managers
* Martin Monperrus Professor @ KTH Royal Institute of Technology
* GHC devs = Haskell
* "python people" (pypi pip...)

* Cargo
* chainguard (wolfi)
* go toolchain team
* Alpine & postmarket OS
* Software heritage
* QEMU
* Red Hat
* Google Android team
* iOS
* Yocto
* UEFI-maybe on Arm/RISC-V

