---
layout: default
title: Who is involved?
permalink: /who/people/
---

# Involved people

← *Back to [who is involved]({{ "/who/" | relative_url }})*.

<br>

Various people in many projects are or have been working together on providing reproducible builds to their users and developers. This is an incomplete list:
{: .lead}

{% if site.data.contributors %}
## Contributors

> {% for x in site.data.contributors %}{{ x }}{% unless forloop.last %}, {% endunless %}{% endfor %}.

If you know someone who should be listed here, please email
[`contact@reproducible-builds.org`](mailto:contact@reproducible-builds.org) or submit a patch as explained in the footer below.

{% endif %}

## Core team

* [Chris Lamb](https://chris-lamb.co.uk) (`lamby`)
* [Holger Levsen](http://layer-acht.org/thinking/) (`h01ger`)
* [Mattia Rizzolo](https://mapreri.org/) (`mapreri`)
* [Vagrant Cascadian](https://www.aikidev.net/about/story/) (`vagrantc`)

The 'core team' consists of those who have been working consistently towards
reproducible builds for many years on both the technical aspects of the project
as well as participating in significant networking around the community itself.
Their work is funded by the Reproducible Builds project via the
[Software Freedom Conservancy](https://sfconservancy.org/).

The preferred way to contact the team is to post to [our public mailing list, `rb-general`](https://lists.reproducible-builds.org/listinfo/rb-general). However, if you wish to contact the core team directly, please email [`contact@reproducible-builds.org`](mailto:contact@reproducible-builds.org).

## Steering Committee

* [Allen Gunn](https://aspirationtech.org)
* [Bdale Garbee](http://gag.com/bdale/)
* [Holger Levsen](http://layer-acht.org/thinking/)
* [Keith Packard](https://keithp.com)
* [Mattia Rizzolo](https://mapreri.org/)
* [Stefano Zacchiroli](https://upsilon.cc/)

The Steering Committee acts as the board of the Reproducible Builds project within the [Software Freedom Conservancy](https://sfconservancy.org/). Its main purpose is deciding how to spend project money.


