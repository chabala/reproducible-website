---
layout: default
title: Our sponsors
permalink: /who/sponsors/
redirect_from:
 - /sponsor/
 - /sponsors/
---

# Sponsors

← *Back to [who is involved]({{ "/who/" | relative_url }})*.

<br>

The not-for-profit Reproducible Builds effort needs your help to continue its work towards ensuring the security of computer systems of all shapes and sizes around the world. We use any and all donated funds to ensure focused and intense work on ensuring this mission.
{: .lead .pb-3}

[![Software Freedom Conservancy]({{ "/assets/images/sponsor/conservancy.png#left" | relative_url }})](https://sfconservancy.org/)

The Reproducible Builds project is a member of the [Software Freedom Conservancy](https://sfconservancy.org/), a [501(c)3](https://en.wikipedia.org/wiki/501(c)(3)_organization) non-profit organisation.
Conservancy has allowed us to pool organisational resources with other projects, such as Selenium, Inkscape, Samba, and Wine, in order to reduce the management overhead associated with creating our own, dedicated legal entity.


If you are interested in the work of the Reproducible Builds project, please consider [becoming a sponsor]({{ "/donate/" | relative_url }}).
{: .lead}


#### Fiscal sponsors

The majority of sponsorship funds go directly towards supporting the Reproducible Builds project, such as development and server expenses. A small portion of the funds are set aside for the Conservancy to continue their work in supporting Reproducible Builds and other open source initiatives.

{% assign levels = "platinum,gold,silver,bronze" | split: ',' %}
{% for level in levels %}
#### {{ level | capitalize }}
{% assign sponsors = site.data.sponsors[level] | sort: 'name' %}
{% if sponsors.size != 0 %}
<div class="row bg-light p-md-4 p-sm-2 pt-5 pb-5">
    {% for x in sponsors %}
    <div class="col-xs-12 col-sm-6 mb-6 pb-3 mx-auto">
        <div class="card text-center h-100">
            <a href="{{ x.url }}" name="{{ x.name }}">
                <img class="p-5 project-img{% if level == "platinum" %} sponsor-img-platinum{% endif %}" src="{{ x.logo | prepend: "/assets/images/sponsors/" | relative_url }}" alt="{{ x.name }}">
            </a>
        </div>
    </div>
    {% endfor %}
</div>
{% else %}
<i>No sponsors at this level.</i>
{% endif %}
{% endfor %}

<br>

{% if site.data.sponsors.none.size != 0 %}
#### Others
<ul>
{% assign sponsors = site.data.sponsors.none | sort: 'name' %}
{% for x in sponsors %}
<li><a href="{{x.url}}" name="{{ x.name }}">{{x.name}}</a></li>
{% endfor %}
</ul>
{% endif %}

#### Non-fiscal sponsors

<div class="row bg-light p-md-4 p-sm-2 pt-5 pb-5">
    {% assign xs = site.data.sponsors_nonfiscal | sort: 'name' %}
    {% for x in xs %}
    <div class="col-xs-12 col-sm-6 mb-6 mx-auto mb-3">
        <div class="card text-center h-100">
            <a href="{{ x.url }}" name="{{ x.name }}">
                <img class="px-5 pt-5 pb-2 project-img" src="{{ x.logo | prepend: "/assets/images/sponsors/" | relative_url }}" alt="{{ x.name }}">
            </a>
            <div class="card-body">
                <small class="text-muted">({{ x.description }})</small>
            </div>
        </div>
    </div>
    {% endfor %}
</div>

#### Former sponsors

We used to sponsored by these entities

<div class="row bg-light p-md-4 p-sm-2 pt-5 pb-5">
    {% assign xs = site.data.sponsors['old'] | sort: 'name' %}
    {% for x in xs %}
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 mb-3 mx-auto">
        <div class="card text-center h-100">
            <a href="{{ x.url }}" name="{{ x.name }}">
                <img class="px-5 pt-5 pb-2 project-img" src="{{ x.logo | prepend: "/assets/images/sponsors/" | relative_url }}" alt="{{ x.name }}">
            </a>
        </div>
    </div>
    {% endfor %}
</div>
